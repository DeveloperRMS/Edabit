﻿namespace Edabit.Allergy.Test;

public class AllegryTests {
    [SetUp]
    public void Setup() {
    }

    [Test]
    public void NoAllegery() {
        string name = "Mary";
        Allergies allergy = new(name);
        Assert.AreEqual(name + " has no allergies!", allergy.ToString());
        Assert.AreEqual(name, allergy.Name);
        Assert.AreEqual(0, allergy.Score);
    }
    [Test]
    public void AllegeryScore() {
        string name = "Joe"; int score = 65;
        Allergies allergy = new(name, score);
        Assert.AreEqual(name, allergy.Name);
        Assert.IsTrue(allergy.IsAllergicTo("Eggs"));
        Assert.IsTrue(allergy.IsAllergicTo("Pollen"));
        Assert.IsFalse(allergy.IsAllergicTo("Peanuts"));
        Assert.AreEqual(name + " is allergic to Eggs and Pollen.", allergy.ToString());
    }
    [Test]
    public void AllegeriesList() {
        string name = "Rob"; string allegeries =  "Peanuts Chocolate Cats Strawberries";
        Allergies allergy = new(name, allegeries);
        Assert.AreEqual(name + " is allergic to Peanuts, Strawberries, Chocolate and Cats.", allergy.ToString());
        Assert.AreEqual(name, allergy.Name);
        Assert.AreEqual(170, allergy.Score);
        Assert.IsTrue(allergy.IsAllergicTo("Peanuts"));
        Assert.IsTrue(allergy.IsAllergicTo("Chocolate"));
        Assert.IsTrue(allergy.IsAllergicTo("Cats"));
        Assert.IsTrue(allergy.IsAllergicTo("Strawberries"));
        Assert.IsFalse(allergy.IsAllergicTo("Eggs"));
    }
    [Test]
    public void AddAllegery() {
        string name = "Mary";
        Allergies allergy = new(name);
        Assert.IsFalse(allergy.IsAllergicTo("Eggs"));
        allergy.AddAllergy("Eggs");
        Assert.AreEqual(name + " is allergic to Eggs.", allergy.ToString());
        Assert.AreEqual(name, allergy.Name);
        Assert.AreEqual(1, allergy.Score);
        Assert.IsTrue(allergy.IsAllergicTo("Eggs"));
    }
    [Test]
    public void DeleteAllergys() {
        string name = "Rob"; string allegeries = "Peanuts Chocolate Cats Strawberries";
        Allergies allergy = new(name, allegeries);
        Assert.IsTrue(allergy.IsAllergicTo("Peanuts"));
        allergy.DeleteAllergy("Peanuts");
        Assert.IsFalse(allergy.IsAllergicTo("Peanuts"));
        Assert.AreEqual(168, allergy.Score);
    }
}
