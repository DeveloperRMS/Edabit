﻿using static Edabit.Allergy.Allergies;

namespace Edabit.Allergy;

interface IAllegery {
    string Name { get; }
    int Score { get; }

    bool IsAllergicTo(string allergy);
    void AddAllergy(Allergen allergy);
    void DeleteAllergy(Allergen allergy);
    string ToString();

}

public class Allergies : IAllegery {
    public string Name { get; private set; }
    public int Score {
        get {
            int score = myAllegaries.Sum(a => (int)a);
            return score;
        }
    }

    private List<Allergen> myAllegaries = new();

    // do not alter this enum
    [Flags]
    public enum Allergen {
        Eggs = 1,
        Peanuts = 2,
        Shellfish = 4,
        Strawberries = 8,
        Tomatoes = 16,
        Chocolate = 32,
        Pollen = 64,
        Cats = 128
    }

    public Allergies(string name, int score = 0) {
        Name = name;
        if (score != 0) SetAllegriesViaScore(score);
    }
    public Allergies(string name, string allegaries) {
        Name = name;
        string[] allegariesArray = allegaries.Split(' ');
        foreach (String allegary in allegariesArray) AddAllergy(allegary);
    }

    private void SetAllegriesViaScore(int score) {
        int tempScore = score;
        if (tempScore >= (int)Allergen.Cats) { tempScore = AddViaScore(Allergen.Cats, tempScore); }
        if (tempScore >= (int)Allergen.Pollen) { tempScore = AddViaScore(Allergen.Pollen, tempScore); }
        if (tempScore >= (int)Allergen.Chocolate) { tempScore = AddViaScore(Allergen.Chocolate, tempScore); }
        if (tempScore >= (int)Allergen.Tomatoes) { tempScore = AddViaScore(Allergen.Tomatoes, tempScore); }
        if (tempScore >= (int)Allergen.Strawberries) { tempScore = AddViaScore(Allergen.Strawberries, tempScore); }
        if (tempScore >= (int)Allergen.Shellfish) { tempScore = AddViaScore(Allergen.Shellfish, tempScore); }
        if (tempScore >= (int)Allergen.Peanuts) { tempScore = AddViaScore(Allergen.Peanuts, tempScore); }
        if (tempScore >= (int)Allergen.Eggs) { AddViaScore(Allergen.Eggs, tempScore); }
    }

    private Int32 AddViaScore(Allergen allergen, int tempScore) {
        AddAllergy(allergen);
        tempScore -= (int)allergen;
        return tempScore;
    }

    private static Allergen AllegenFromStr(String allergy) {       
        Allergen all = (Allergen)Enum.Parse(typeof(Allergen), allergy);;
        return all;
    }
    private static string ReplaceLastCommaByAnd(string source) {
        int place = source.LastIndexOf(",");
        if (place == -1) return source;
        return source.Remove(place, 1).Insert(place, " and");
    }


    public void AddAllergy(Allergen allergy) => myAllegaries.Add(allergy);
    public void AddAllergy(string allergy) => AddAllergy(AllegenFromStr(allergy));
    public void DeleteAllergy(string allergy) => DeleteAllergy(AllegenFromStr(allergy));
    public void DeleteAllergy(Allergen allergy) => myAllegaries.Remove(allergy);
    public bool IsAllergicTo(string allergy) => IsAllergicTo(AllegenFromStr(allergy));
    public bool IsAllergicTo(Allergen allergy) => myAllegaries.Contains(allergy);


    public override string ToString() {
        myAllegaries = myAllegaries.OrderBy(a => (int)a).ToList();

        String stringList = string.Join(", ", myAllegaries.ToArray());
        String stringListAnd = ReplaceLastCommaByAnd(stringList);
        string allegiesStr = myAllegaries.Count == 0
            ? "has no allergies!"
            : "is allergic to " + stringListAnd + ".";

        return Name + " " + allegiesStr;
    }
}