using Microsoft.VisualStudio.TestPlatform.TestHost;

namespace EdaBit.League.Test {
    public class UnitTest1 {
        string[] _table = new string[] {
            "Arsenal, 38, 14, 14, 10, 8",
            "Aston Villa, 38, 9, 8, 21, -26",
            "Bournemouth, 38, 9, 7, 22, -26",
            "Brighton, 38, 9, 14, 15, -15",
            "Burnley, 38, 15, 9, 14, -7",
            "Chelsea, 38, 20, 6, 12, 15",
            "Crystal Palace, 38, 11, 10, 17, -19",
            "Everton, 38, 13, 10, 15, -12",
            "Leicester City, 38, 18, 8, 12, 26",
            "Liverpool, 38, 32, 3, 3, 52",
            "Manchester City, 38, 26, 3, 9, 67",
            "Manchester Utd, 38, 18, 12, 8, 30",
            "Newcastle, 38, 11, 11, 16, -20",
            "Norwich, 38, 5, 6, 27, -49",
            "Sheffield Utd, 38, 14, 12, 12, 0",
            "Southampton, 38, 15, 7, 16, -9",
            "Tottenham, 38, 16, 11, 11, 14",
            "Watford, 38, 8, 10, 20, -28",
            "West Ham, 38, 10, 9, 19, -13",
            "Wolves, 38, 15, 14, 9, 1"
        };

    [Fact]
        public void Test1() {
            Assert.Equal("Liverpool came 1st with 99 points and a goal difference of 52!", League.EPLResult(_table, "Liverpool"));
            Assert.Equal("Manchester Utd came 3rd with 66 points and a goal difference of 30!", League.EPLResult(_table, "Manchester Utd"));
            Assert.Equal("Norwich came 20th with 21 points and a goal difference of -49!", League.EPLResult(_table, "Norwich"));
            Assert.Equal("Tottenham came 6th with 59 points and a goal difference of 14!", League.EPLResult(_table, "Tottenham"));
            Assert.Equal("Aston Villa came 17th with 35 points and a goal difference of -26!", League.EPLResult(_table, "Aston Villa"));
            Assert.Equal("Southampton came 11th with 52 points and a goal difference of -9!", League.EPLResult(_table, "Southampton"));
            Assert.Equal("Manchester City came 2nd with 81 points and a goal difference of 67!", League.EPLResult(_table, "Manchester City"));
        }
        [Fact]
        public void LeagueResult() {
            LeagueResult Arsenal = new(_table[0]);
            Assert.Equal(Arsenal.Team, "Arsenal");
            Assert.Equal(Arsenal.Played, 38);
            Assert.Equal(Arsenal.Won, 14);
            Assert.Equal(Arsenal.Drawn, 14);
            Assert.Equal(Arsenal.Lost, 10);
            Assert.Equal(Arsenal.Gdiff, 8);
            Assert.Equal(Arsenal.Score, 14*3+14);
        }
        [Fact]
        public void Rank() {
            Assert.Equal(League.Rank(1), "1st");
            Assert.Equal(League.Rank(2), "2nd");
            Assert.Equal(League.Rank(3), "3rd");
            Assert.Equal(League.Rank(4), "4th");
            Assert.Equal(League.Rank(5), "5th");
        }
    }
}