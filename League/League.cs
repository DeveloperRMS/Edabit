﻿namespace EdaBit.League {

    public class LeagueResult {

        public string Team { get; set; }
        public int Played { get; set; }
        public int Won { get; set; }
        public int Drawn { get; set; }
        public int Lost { get; set; }
        public int Gdiff { get; set; }
        public int Score {
            get {
                return Won * 3 + Drawn;
            }
        }

        public LeagueResult(string tableRow) {
            string[] elements = tableRow.Split(',');
            Team = elements[0];
            Played = int.Parse(elements[1]);
            Won = int.Parse(elements[2]);
            Drawn = int.Parse(elements[3]);
            Lost = int.Parse(elements[4]);
            Gdiff = int.Parse(elements[5]);
        }
    }


    public static class League {

        public static string EPLResult(string[] table, string team) {
            List<LeagueResult> results = table
                .Select(row => new LeagueResult(row))
                .OrderByDescending(row => row.Score).ThenByDescending(row => row.Gdiff)
                .ToList<LeagueResult>();
            LeagueResult? teamResult = results.FirstOrDefault(result => result.Team == team);
            if (teamResult == null) return "";



            int placeNr = results.IndexOf(teamResult) + 1;
            string place = Rank(placeNr);

            return teamResult.Team + " came " + place + " with " + teamResult.Score + " points and a goal difference of " + teamResult.Gdiff + "!";

        }
        public static String Rank(Int32 placeNr) {
            switch (placeNr) {
                case 1: return placeNr + "st";
                case 2: return placeNr + "nd";
                case 3: return placeNr + "rd";
                
                default:return placeNr + "th";
            }            
        }

    }

}