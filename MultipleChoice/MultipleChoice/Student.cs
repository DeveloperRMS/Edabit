namespace Edabit.MultipleChoice {

public interface IStudent
{
  string[] TestsTaken { get; }
  void TakeTest(ITestpaper paper, string[] answers);
}

public class Student : IStudent
{

  private List<TestResult> _testsTaken = new();

  public string[] TestsTaken
  {
    get
    {
      if (_testsTaken.Count == 0) return new string[] { "No tests taken" };

      string[] results = _testsTaken
      .OrderBy(result => result.Subject)
      .Select(result => result.ToString())
      .ToArray();

      return results;
    }
  }

  public void TakeTest(ITestpaper paper, string[] answers)
  {
    TestResult result = paper.Grade(answers);
    _testsTaken.Add(result);
  }
}
}
