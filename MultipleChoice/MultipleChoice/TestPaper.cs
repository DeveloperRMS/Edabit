namespace Edabit.MultipleChoice
{
  public interface ITestpaper {
    string Subject { get; }
    string[] MarkScheme { get; }
    string PassMark { get; }
    TestResult Grade(string[] answers);
  }

  public class Testpaper : ITestpaper {
    public string Subject { get; set; }
    public string[] MarkScheme { get; set; }
    public string PassMark { get; set; }

    public Testpaper(string subject, string[] markScheme, string passMark) {
      Subject = subject;
      MarkScheme = markScheme;
      PassMark = passMark;
    }

    public TestResult Grade(string[] answers) {
      int amountQuestions = MarkScheme.Length;
      int correctAnswers = answers.Where((answer, index) => answer == MarkScheme[index]).Count();

      double pct = 100.0 / amountQuestions * correctAnswers;
      int grade = (int)Math.Round(pct);
      int passPct = int.Parse(PassMark.Replace("%", ""));
      TestResult result = new(Subject, grade, passPct);
      return result;
    }
  }
}