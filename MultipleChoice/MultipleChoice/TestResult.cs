namespace Edabit.MultipleChoice {
  public class TestResult {
    public string Subject { get; private set; }
    public string PassOrFailed { get; private set; }
    public int Pct { get; private set; }

    public TestResult(string sub, int grade, int passMark) {
      Subject = sub;
      Pct = grade;
      PassOrFailed = grade >= passMark ? "Passed!" : "Failed!";
    }

    public override string ToString() {
      return $"{Subject}: {PassOrFailed} ({Pct}%)";
    }
  }
}