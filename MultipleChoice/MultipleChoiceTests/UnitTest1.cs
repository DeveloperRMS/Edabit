﻿namespace Edabit.MultipleChoice.Tests;

public class UnitTests
{
    [SetUp]
    public void Setup()
    {
    }

    [Test]
    public void StudentWithoutTests()
    {
    Student student = new();
    Assert.That(student.TestsTaken,
      Is.EqualTo(new string[] { "No tests taken" }));
    }
}
