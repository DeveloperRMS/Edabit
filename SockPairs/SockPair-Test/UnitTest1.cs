

namespace Edabit.SockPairs.Test {
    public class UnitTest1 {
        [Fact]
        public void OnePair() {
            int pairs = Socks.Pairs("AA");
            Assert.Equal(1, pairs);
        }
        [Fact]
        public void TwoPair() {
            int pairs = Socks.Pairs("ABAB");
            Assert.Equal(2, pairs);
        }
        [Fact]
        public void MultipleSamePairs() {
            int pairs = Socks.Pairs("AABBAA");
            Assert.Equal(3, pairs);
        }
    }
}