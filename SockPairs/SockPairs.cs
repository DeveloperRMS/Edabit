﻿namespace Edabit.SockPairs {
    public static class Socks {

        public static int Pairs(string socks) {
            Dictionary<char, int> pairs = new();

            foreach (char sock in socks) {
                if (pairs.Keys.Contains(sock)) pairs[sock]++;
                else pairs.Add(sock, 1);
            }

            int totalPairs = pairs.Sum(s => s.Value/2);
            return totalPairs;
        }

    }
}