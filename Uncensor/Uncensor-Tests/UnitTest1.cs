namespace Edabit.Uncensor.Tests {
    public class Uncensor {
        [Fact]
        public void VowelsGiven() {
            string result = UncensorVowels.Uncensor("Wh*r* d*d my v*w*ls g*?", "eeioeo");
            Assert.Equal("Where did my vowels go?", result);
        }
        [Fact]
        public void NoEndPart() {
            string result = UncensorVowels.Uncensor("Wh*r*", "ee");
            Assert.Equal("Where", result);
        }
        [Fact]
        public void WithoutVowels() {
            string result = UncensorVowels.Uncensor("abcd", "");
            Assert.Equal("abcd", result);
        }
        [Fact]
        public void Uppercase() {
            string result = UncensorVowels.Uncensor("*PP*RC*S*", "UEAE");
            Assert.Equal("UPPERCASE", result);
        }
    }
}