﻿using System;

namespace Edabit.Uncensor {
    public static class UncensorVowels {

        public static string Uncensor (string censored,string vowelsReplaced) {
            char[] vowels=vowelsReplaced.ToCharArray();
            string result = "";
            for(int i = 0; i < vowels.Length; i++) {
                result += censored.Split('*')[i] + vowels[i];
            }
            int parts = censored.Split('*').Length;
            result =parts ==vowels.Length 
                ? result
                : result + censored.Split('*')[parts-1] ;

            return result;
        }

    }
}